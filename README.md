The OpenAMEE Platform
=====================

Running the platform locally
----------------------------

* Create an IntelliJ run configuration
  * Main class: `com.amee.engine.RestletEngine`
  * Working directory: `/path/to/amee-platform`
* Mark `amee-platform-core/src/main/config` as a Sources Root
* Configure amee.properties


Creating the index
------------------

* Use the following properties to create/update the index:
  * `index.clear`
  * `index.checkDataCategories`
  * `index.indexDataCategories`
  * `index.indexDataItems`


Packaging the platform
----------------------

```sh
mvn clean package -P tar
```
