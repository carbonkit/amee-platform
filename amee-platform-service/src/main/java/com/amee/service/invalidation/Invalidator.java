package com.amee.service.invalidation;

import com.amee.domain.AMEEEntityReference;
import com.amee.domain.ObjectType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import org.apache.commons.cli.*;

import java.io.IOException;

public class Invalidator {

    private static final Invalidator INSTANCE = new Invalidator();

    private Invalidator() {
        super();
    }

    public static void main(String[] args) {
        try {
            INSTANCE.sendMessage(args);
        } catch (IOException e) {
            if (e.getCause() == null) {
                System.out.println("Caught IOException: " + e.getMessage());
            } else {
                System.out.println("Caught IOException: " + e.getMessage() + " Cause: " + e.getCause().getMessage());
            }
        }
    }

    private void sendMessage(String[] args) throws IOException {

        String scope = null;
        String host = "localhost";
        String port = "5672";
        String username = null;
        String password = null;
        String entityType = "DC";
        String entityUid = null;
        String invOptions = null;

        CommandLine line = null;
        CommandLineParser parser = new DefaultParser();
        Options options = new Options();

        // Define scope option.
        Option scopeOpt = Option.builder("scope")
                .argName("scope")
                .hasArg()
                .required()
                .desc("The messaging scope name (e.g., 'live', 'science' or 'stage'). Required")
                .build();
        options.addOption(scopeOpt);

        // Define host option.
        Option hostOpt = Option.builder("host")
                .argName("host")
                .hasArg()
                .desc("The RabbitMQ host name. Optional, defaults to 'localhost'.")
                .build();
        options.addOption(hostOpt);

        // Define port option.
        Option portOpt = Option.builder("port")
                .argName("port")
                .hasArg()
                .desc("The RabbitMQ port number. Optional, defaults to '5672'.")
                .build();
        options.addOption(portOpt);

        // Define username option.
        Option usernameOpt = Option.builder("username")
                .argName("username")
                .hasArg()
                .required()
                .desc("The RabbitMQ username. Required.")
                .build();
        options.addOption(usernameOpt);

        // Define password option.
        Option passwordOpt = Option.builder("password")
                .argName("password")
                .hasArg()
                .required()
                .desc("The RabbitMQ password. Required.")
                .build();
        options.addOption(passwordOpt);

        // Define entityType option.
        Option entityTypeOpt = Option.builder("entityType")
                .argName("entityType")
                .hasArg()
                .desc("The entity type to invalidate. Optional, defaults to 'DC'.")
                .build();
        options.addOption(entityTypeOpt);

        // Define entityUids option.
        Option entityUidsOpt = Option.builder("entityUid")
                .argName("entityUid")
                .hasArg()
                .required()
                .desc("The entity UIDs to invalidate. This can be a single UID or a CSV list (e.g., 'AAAAAAAAAAAA,BBBBBBBBBBBB'). Required.")
                .build();
        options.addOption(entityUidsOpt);

        // Define entityUid option.
        Option invOptionsOpt = Option.builder("invOptions")
                .argName("invOptions")
                .hasArg()
                .desc("Extra options for the invalidation message. This can be a single option or a CSV list (e.g., 'indexDataItems'). Optional.")
                .build();
        options.addOption(invOptionsOpt);

        // Parse the options.
        try {
            line = parser.parse(options, args);
        } catch (ParseException exp) {
            new HelpFormatter().printHelp("java " + this.getClass().getName(), options);
            System.exit(-1);
        }

        // Handle scope.
        if (line.hasOption(scopeOpt.getOpt())) {
            scope = line.getOptionValue(scopeOpt.getOpt());
        }

        // Handle host.
        if (line.hasOption(hostOpt.getOpt())) {
            host = line.getOptionValue(hostOpt.getOpt());
        }

        // Handle port.
        if (line.hasOption(portOpt.getOpt())) {
            port = line.getOptionValue(portOpt.getOpt());
        }

        // Handle username.
        if (line.hasOption(usernameOpt.getOpt())) {
            username = line.getOptionValue(usernameOpt.getOpt());
        }

        // Handle password.
        if (line.hasOption(passwordOpt.getOpt())) {
            password = line.getOptionValue(passwordOpt.getOpt());
        }

        // Handle entityType.
        if (line.hasOption(entityTypeOpt.getOpt())) {
            entityType = line.getOptionValue(entityTypeOpt.getOpt());
        }

        // Handle entityUid.
        if (line.hasOption(entityUidsOpt.getOpt())) {
            entityUid = line.getOptionValue(entityUidsOpt.getOpt());
        }

        // Handle invOptions.
        if (line.hasOption(invOptionsOpt.getOpt())) {
            invOptions = line.getOptionValue(invOptionsOpt.getOpt());
        }

        // Send the message!
        sendMessage(scope, host, port, username, password, entityType, entityUid, invOptions);
    }

    private void sendMessage(
            String scope,
            String hostName,
            String portNumber,
            String username,
            String password,
            String entityType,
            String entityUids,
            String invOptions) throws IOException {
        System.out.println("    Configure RabbitMQ.");
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        connectionFactory.setHost(hostName);
        connectionFactory.setPort(Integer.valueOf(portNumber));
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();
        System.out.println("    Send the messages.");
        for (String entityUid : entityUids.split(",")) {
            System.out.println("    Create InvalidationMessages.");
            InvalidationMessage message =
                    new InvalidationMessage(
                            this,
                            new AMEEEntityReference(ObjectType.fromString(entityType), entityUid),
                            invOptions);
            String messageText = message.getMessage();
            System.out.println("        Message: " + messageText);
            System.out.println("        Destination: platform." + scope + ".invalidation");
            channel.basicPublish(
                    "platform." + scope + ".invalidation",
                    "platform." + scope + ".invalidation",
                    false,
                    false,
                    MessageProperties.PERSISTENT_TEXT_PLAIN,
                    messageText.getBytes());
        }
        System.out.println("    Close the channel & connection.");
        channel.close();
        connection.close();
    }
}
