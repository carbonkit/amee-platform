package com.amee.service.auth;

import com.amee.base.domain.ResultsWrapper;
import com.amee.domain.UsersFilter;
import com.amee.domain.auth.User;
import com.amee.domain.site.ISite;
import com.amee.service.invalidation.InvalidationMessage;
import org.springframework.context.ApplicationListener;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface AuthenticationService extends ApplicationListener<InvalidationMessage> {

    // authToken header and cookie name.
    String AUTH_TOKEN = "authToken";

    User doGuestSignIn();

    String isAuthenticated(ISite site, String authToken, String remoteAddress);

    /**
     * Get the current active user from the supplied AuthToken.
     *
     * @param authToken representing the active user.
     * @return the active user
     */
    User getActiveUser(String authToken);

    /**
     * Authenticates based on the supplied sample user. The sample user must have a username and
     * password set. If authentication is successful the persistent User is returned.
     *
     * @param sampleUser sample User to authenticate against
     * @return the authenticated User
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    User authenticate(User sampleUser, String rawPassword);

    String generateAuthToken(User activeUser, String remoteAddress);

    User getUserByUid(String uid);

    User getUserByUsername(String username);

    User getUserByIdentifier(String identifier);

    ResultsWrapper<User> getUsers(UsersFilter filter);

    /**
     * Clears local caches.
     *
     * @param user the User to clear from the cache.
     */
    void clearCaches(User user);

    void persist(User user);

    void remove(User user);

    /**
     * Returns true if the username of the supplied User is unique.
     *
     * @param user the user to check for uniqueness
     * @return true if the User has a unique username
     */
    boolean isUserUniqueByUsername(User user);

    /**
     * Returns true if the email of the supplied User is unique.
     *
     * @param user the user to check for uniqueness
     * @return true if the User has a unique email address
     */
    boolean isUserUniqueByEmail(User user);

}
