package com.amee.service.auth;

import com.amee.base.domain.ResultsWrapper;
import com.amee.domain.AMEEStatus;
import com.amee.domain.UsersFilter;
import com.amee.domain.auth.User;
import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class AuthenticationDAO {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private static final String CACHE_REGION = "query.authenticationService";

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings(value = "unchecked")
    public User getUserByUid(String uid) {
        List<User> users = entityManager.createQuery(
                "SELECT DISTINCT u " +
                        "FROM User u " +
                        "WHERE u.uid = :userUid " +
                        "AND u.status <> :trash")
                .setParameter("userUid", uid)
                .setParameter("trash", AMEEStatus.TRASH)
                .setHint("org.hibernate.cacheable", true)
                .setHint("org.hibernate.cacheRegion", CACHE_REGION)
                .getResultList();
        if (users.size() == 1) {
            log.debug("auth found: {}", uid);
            return users.get(0);
        }
        log.debug("auth NOT found: {}", uid);
        return null;
    }

    @SuppressWarnings(value = "unchecked")
    public User getUserByUsername(String username) {
        List<User> users = entityManager.createQuery(
                "SELECT DISTINCT u " +
                        "FROM User u " +
                        "WHERE u.username = :username " +
                        "AND u.status <> :trash")
                .setParameter("username", username)
                .setParameter("trash", AMEEStatus.TRASH)
                .setHint("org.hibernate.cacheable", true)
                .setHint("org.hibernate.cacheRegion", CACHE_REGION)
                .getResultList();
        if (users.size() == 1) {
            log.debug("auth found: {}", username);
            return users.get(0);
        }
        log.debug("auth NOT found: {}", username);
        return null;
    }

    public ResultsWrapper<User> getUsers(UsersFilter filter) {

        // Create Criteria.
        // See http://www.hibernate.org/117.html#A12 for notes on DISTINCT_ROOT_ENTITY.
        Session session = (Session) entityManager.getDelegate();
        Criteria criteria = session.createCriteria(User.class);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        if (filter.getEmail() != null) {
            criteria.add(Restrictions.ilike("email", filter.getEmail(), MatchMode.EXACT));
        }
        if (filter.getPasswordToken() != null) {
            criteria.add(Restrictions.ilike("passwordToken", filter.getPasswordToken(), MatchMode.EXACT));
        }
        criteria.add(Restrictions.ne("status", AMEEStatus.TRASH));
        criteria.addOrder(Order.asc("username").ignoreCase());
        criteria.setCacheable(true);
        criteria.setCacheRegion(CACHE_REGION);

        // Apply start and limit if relevant.
        if (filter.getResultStart() > 0) {
            criteria.setFirstResult(filter.getResultStart());
        }
        if (filter.getResultLimit() > 0) {
            criteria.setMaxResults(filter.getResultLimit() + 1);
        }

        // Get the results.
        @SuppressWarnings("unchecked")
        List<User> users = (List<User>) criteria.list();
        if (filter.getResultLimit() > 0) {
            return new ResultsWrapper<>(
                    users.size() > filter.getResultLimit() ? users.subList(0, filter.getResultLimit()) : users,
                    users.size() > filter.getResultLimit());
        } else {
            return new ResultsWrapper<>(users, false);
        }
    }

    /**
     * Clears the hibernate cache for the given User.
     *
     * @param user the User to clear from the cache.
     */
    public void invalidate(User user) {
        log.debug("invalidate() {}", user);
        ((Session) entityManager.getDelegate()).getSessionFactory().getCache().evictEntity(User.class, user.getId());
    }

    public void persist(User user) {
        entityManager.persist(user);
    }

    /**
     * Returns true if the username of the supplied User is unique.
     *
     * @param user the user to check for uniqueness
     * @return true if the User has a unique username
     */
    public boolean isUserUniqueByUsername(User user) {
        if (user != null) {
            Session session = (Session) entityManager.getDelegate();
            Criteria criteria = session.createCriteria(User.class);
            if (entityManager.contains(user)) {
                criteria.add(Restrictions.ne("uid", user.getUid()));
            }
            criteria.add(Restrictions.eq("username", user.getUsername()));
            criteria.add(Restrictions.ne("status", AMEEStatus.TRASH));
            criteria.setFlushMode(FlushMode.MANUAL);
            return criteria.list().isEmpty();
        } else {
            throw new RuntimeException("User was null.");
        }
    }

    /**
     * Returns true if the email address of the supplied User is unique.
     *
     * @param user the user to check for uniqueness
     * @return true if the User has a unique email address
     */
    public boolean isUserUniqueByEmail(User user) {
        if (user != null) {
            Session session = (Session) entityManager.getDelegate();
            Criteria criteria = session.createCriteria(User.class);
            if (entityManager.contains(user)) {
                criteria.add(Restrictions.ne("uid", user.getUid()));
            }
            criteria.add(Restrictions.eq("email", user.getEmail()));
            criteria.add(Restrictions.ne("status", AMEEStatus.TRASH));
            criteria.setFlushMode(FlushMode.MANUAL);
            return criteria.list().isEmpty();
        } else {
            throw new RuntimeException("User was null.");
        }
    }

    /**
     * Removes (trashes) a User.
     *
     * @param user to remove
     */
    public void remove(User user) {
        user.setStatus(AMEEStatus.TRASH);
    }
}
