package com.amee.integration

import com.amee.base.utils.UidGen
import com.amee.domain.auth.User
import com.amee.service.auth.AuthenticationDAO
import groovyx.net.http.HttpResponseException
import org.junit.Test

import static groovyx.net.http.ContentType.*
import static org.junit.Assert.fail
import static org.restlet.data.Status.*

/**
 * Created by david on 27/01/2016.
 *
 * Tests for the User API.
 */
class UserIT extends BaseApiTest {

    /**
     * Tests for creation, fetch and deletion of a user using JSON & XML responses.
     *
     * Create a new user by POSTing to '/users'
     *
     * Supported POST parameters are:
     *
     * <ul>
     * <li>name
     * <li>email
     * <li>username
     * <li>password
     * <li>groups
     * <li>cloneUserUid
     * </ul>
     *
     * NOTE: For detailed rules on these parameters see the validation tests below.
     *
     * Delete (TRASH) a user by sending a DELETE request to '/users/{UID|username}'.
     */
    @Test
    void createAndRemoveUser() {
        versions.each { version -> createAndRemoveUser(version) }
    }

    def createAndRemoveUser(version) {
        createAndRemoveUserJson(version)
        createAndRemoveUserXml(version)
    }

    def createAndRemoveUserJson(version) {
        if (version >= 3.6) {
            setAdminUser()

            def name = 'User to be deleted'

            // Create a new user
            def responsePost = client.post(
                    path: "/$version/users",
                    body: [username: 'foo',
                           password: 'bar',
                           email: 'FOO@example.com',
                           name: name,
                           cloneUserUid: '36C95YJJALQP'],
                    requestContentType: URLENC,
                    contentType: JSON)

            // Get and check the location.
            String userLocation = responsePost.headers['Location'].value
            String userUid = userLocation.split('/')[5]
            assert UidGen.INSTANCE_12.isValid(userUid)
            assertOkJson(responsePost, SUCCESS_CREATED.code, userUid)

            // Fetch the User.
            def response = client.get(path: "$userLocation;full", contentType: JSON)
            assert response.status == SUCCESS_OK.code
            assert response.contentType == 'application/json'
            assert response.data.status == 'OK'
            assert response.data.user.name == name

            // Email address should be converted to lowercase
            assert response.data.user.email == 'foo@example.com'

            // We don't want to return the password
            assert response.data.password == null

            // TODO: Check groups when implemented in response

            // Then delete the User.
            def responseDelete = client.delete(path: userLocation)
            assertOkJson(responseDelete, SUCCESS_OK.code, userUid)

            // We should get a 404 here.
            try {
                client.get(path: userLocation)
                fail 'Should have thrown an exception'
            } catch (HttpResponseException e) {
                assert e.response.status == CLIENT_ERROR_NOT_FOUND.code
            }
        }
    }

    def createAndRemoveUserXml(version) {
        if (version >= 3.6) {
            setAdminUser()

            def name = 'User to be deleted'

            // Create a new User.
            def responsePost = client.post(
                    path: "/$version/users",
                    body: [username: 'foo',
                           password: 'bar',
                           email: 'FOO@example.com',
                           name: name,
                           groups: "Administrators,amee"],
                    requestContentType: URLENC,
                    contentType: XML)

            // Get and check the Location.
            String userLocation = responsePost.headers['Location'].value
            String userUid = userLocation.split('/')[5]
            assert UidGen.INSTANCE_12.isValid(userUid)
            assertOkXml(responsePost, SUCCESS_CREATED.code, userUid)

            // Fetch the User.
            def responseGet = client.get(path: "$userLocation;full", contentType: XML)
            assert responseGet.status == SUCCESS_OK.code
            assert responseGet.contentType == 'application/xml'
            assert responseGet.data.Status.text() == 'OK'
            assert responseGet.data.User.@uid.text() == userUid
            assert responseGet.data.User.Name.text() == name

            // Email address should be converted to lowercase
            assert responseGet.data.User.Email.text() == 'foo@example.com'

            // TODO: Check groups when implemented in response

            // Delete it
            def responseDelete = client.delete(path: userLocation, contentType: XML)
            assertOkXml(responseDelete, SUCCESS_OK.code, userUid)

            // Check it has been deleted
            try {
                client.get(path: userLocation)
                fail 'Should have thrown Exception'
            } catch (HttpResponseException e) {
                assert e.response.status == CLIENT_ERROR_NOT_FOUND.code
            }
        }
    }

    /**
     * Tests fetching a single user using JSON & XML.
     *
     * User GET requests support the following matrix parameters to modify the response.
     *
     * <ul>
     * <li>full - include all values.
     * <li>audit - include the status, created and modified values.
     * <li>name - include the name value.
     * <li>email - include the email value.
     * </ul>
     *
     * By default the user UID and username are included.
     * We never return the password or passwordToken.
     */
    @Test
    void getUser() {
        versions.each { version -> getUser(version) }
    }

    def getUser(version) {
        getUserJson(version)
        getUserXml(version)
    }

    def getUserJson(version) {
        if (version >= 3.6) {
            setAdminUser()

            // Fetch by UID
            def response = client.get(path: "/$version/users/3D7D11293ECA;full", contentType: JSON)
            assert response.status == SUCCESS_OK.code
            assert response.contentType == 'application/json'
            assert response.data.status == 'OK'
            assert response.data.user.uid == '3D7D11293ECA'
            assert response.data.user.name == 'Standard'
            assert response.data.user.email == 'standard@example.com'
            assert response.data.user.username == 'standard'
            assert response.data.user.password == null
            assert response.data.user.passwordToken == null
        }
    }

    def getUserXml(version) {
        if (version >= 3.6) {
            setAdminUser()

            // Fetch by name
            def response = client.get(path: "/$version/users/standard;full", contentType: XML)
            assert response.status == SUCCESS_OK.code
            assert response.contentType == 'application/xml'
            assert response.data.Status.text() == 'OK'
            assert response.data.User.@uid.text() == '3D7D11293ECA'
            assert response.data.User.Name.text() == 'Standard'
            assert response.data.User.Email.text() == 'standard@example.com'
            assert response.data.User.Username.text() == 'standard'

            // Awkward way of checking a node doesn't exist: http://stackoverflow.com/a/482264
            assert "Password" != response.data.User.children().find( {it.name() == "Password"})?.name()
            assert "PasswordToken" != response.data.User.children().find( {it.name() == "PasswordToken"})?.name()
        }
    }

    /**
     * A standard user should be able to fetch their own account.
     */
    @Test
    void getSelf() {
        versions.each { version -> getSelf(version) }
    }

    def getSelf(version) {
        if (version >= 3.6) {
            def response = client.get(path: "/$version/users/standard;full", contentType: JSON)
            assert response.status == SUCCESS_OK.code
            assert response.contentType == 'application/json'
            assert response.data.status == 'OK'
            assert response.data.user.uid == '3D7D11293ECA'
            assert response.data.user.name == 'Standard'
            assert response.data.user.email == 'standard@example.com'
            assert response.data.user.username == 'standard'
        }
    }

    /**
     * A standard user should NOT be able to fetch another user's account.
     */
    @Test
    void getOtherUser() {
        versions.each { version -> getOtherUser(version) }
    }

    def getOtherUser(version) {
        if (version >= 3.6) {
            try {
                client.get(path: "/$version/users/other;full", contentType: JSON)

            } catch (HttpResponseException e) {
                def response = e.response
                assert response.status == CLIENT_ERROR_FORBIDDEN.code
                assert response.contentType == 'application/json'
            }
        }
    }

    /**
     * Use the special identifier "me" to fetch the current user.
     */
    @Test
    void getMe() {
        versions.each { version -> getMe(version) }
    }

    def getMe(version) {
        if (version >= 3.6) {
            def response = client.get(path: "/$version/users/me;full", contentType: JSON)
            assert response.status == SUCCESS_OK.code
            assert response.contentType == 'application/json'
            assert response.data.status == 'OK'
            assert response.data.user.uid == '3D7D11293ECA'
            assert response.data.user.name == 'Standard'
            assert response.data.user.email == 'standard@example.com'
            assert response.data.user.username == 'standard'
        }
    }

    /**
     * Tests fetching a list of all users using JSON.
     *
     * User GET requests support the following query parameters to filter the results.
     *
     * <ul>
     * <li>email - The user's email address (exact match).
     * <li>passwordToken - The user's passwordToken (exact match).
     * </ul>
     *
     * Users are sorted by username.
     *
     */
    @Test
    void getAllUsersJson() {
        versions.each { version -> getAllUsersJson(version) }
    }

    def getAllUsersJson(version) {
        if (version >= 3.6) {
            setAdminUser()

            client.contentType = JSON
            def response = client.get(path: "/$version/users;full")
            assert response.status == SUCCESS_OK.code
            assert response.contentType == 'application/json'
            assert response.data.status == 'OK'
            def allUsers = response.data.users
            assert allUsers.size() == 4

            // Users are sorted by username
            assert allUsers.collect { it.username } == allUsers.collect { it.username }.sort { a, b -> a.compareToIgnoreCase(b) }
        }
    }

    @Test
    void getAllUsersXml() {
        versions.each { version -> getAllUsersXml(version) }
    }

    def getAllUsersXml(version) {
        if (version >= 3.6) {
            setAdminUser()

            client.contentType = XML
            def response = client.get(path: "/$version/users;full")
            assert response.contentType == 'application/xml'
            assert response.data.Status.text() == 'OK'
            def allUsers = response.data.Users.children()
            assert allUsers.size() == 4

            // Users are sorted by username
            assert allUsers.Username*.text() == allUsers.Username*.text().sort { a, b -> a.compareToIgnoreCase(b) }
        }
    }

    @Test
    void findUsersByEmail() {
        versions.each { version -> findUsersByEmail(version) }
    }

    def findUsersByEmail(version) {
        if (version >= 3.6) {
            setAdminUser()

            client.contentType = JSON
            def response = client.get(path: "/$version/users", query: [email: 'other@example.com'])
            assert response.status == SUCCESS_OK.code
            assert response.contentType == 'application/json'
            assert response.data.status == 'OK'
            def allUsers = response.data.users
            assert allUsers.size() == 1
            assert allUsers.first().uid == 'NNT6GE88IUN7'
        }
    }

    @Test
    void findUsersByPasswordToken() {
        versions.each { version -> findUsersByPasswordToken(version) }
    }

    def findUsersByPasswordToken(version) {
        if (version >= 3.6) {
            setAdminUser()

            client.contentType = JSON
            def response = client.get(path: "/$version/users", query: [passwordToken: 'VkY1N1ozSjNWMThYOjU0Y2FhNTdmNDljNjFkNGJhMzEzMDg3YTUzNGM0MTM5Y2QwN2Q4MzQxMWNlOGQzN2JhMmRlZmM2NTQ4YjQwM2E6MTQ1Njk1NzEzMjM0Mw=='])
            assert response.status == SUCCESS_OK.code
            assert response.contentType == 'application/json'
            assert response.data.status == 'OK'
            def allUsers = response.data.users
            assert allUsers.size() == 1
            assert allUsers.first().uid == '3D7D11293ECA'
        }
    }

    /**
     * A standard user should NOT be able to fetch all users.
     */
    @Test
    void getAllUsersStandardUser() {
        versions.each { version -> getAllUsersStandardUser(version) }
    }

    def getAllUsersStandardUser(version) {
        if (version >= 3.6) {
            try {
                client.get(path: "/$version/users", contentType: JSON)

            } catch (HttpResponseException e) {
                def response = e.response
                assert response.status == CLIENT_ERROR_FORBIDDEN.code
                assert response.contentType == 'application/json'
            }
        }
    }

    @Test
    void clearPasswordToken() {
        versions.each { version -> clearPasswordToken(version) }
    }

    def clearPasswordToken(version) {
        if (version >= 3.6) {

            // Clear the passwordToken
            def responsePut = client.put(
                    path: "/$version/users/3D7D11293ECA",
                    body: [passwordToken:null],
                    requestContentType: URLENC,
                    contentType: JSON)
            assertOkJson(responsePut, SUCCESS_OK.code, '3D7D11293ECA')

            // Verify it is now null in the database
            AuthenticationDAO dao = context.getBean(AuthenticationDAO)
            def user = dao.getUserByUid('3D7D11293ECA')
            assert user.passwordToken == null
        }
    }

    @Test
    void updateWithInvalidEmail() {
        setAdminUser()
        updateUserFieldJson('email', 'empty', '')
        updateUserFieldJson('email', 'long', String.randomString(User.EMAIL_SIZE + 1))
        updateUserFieldJson('email', 'duplicate', 'other@example.com')
    }

    @Test
    void updateWithInvalidName() {
        setAdminUser()
        updateUserFieldJson('name', 'empty', '')
        updateUserFieldJson('name', 'long', String.randomString(User.NAME_SIZE + 1))
    }

    @Test
    void updateWithInvalidUsername() {
        setAdminUser()
        updateUserFieldJson('username', 'empty', '')
        updateUserFieldJson('username', 'long', String.randomString(User.EMAIL_SIZE + 1))
        updateUserFieldJson('username', 'format', 'invalidChars!!!')
        updateUserFieldJson('username', 'duplicate', 'other')
    }

    @Test
    void updateWithInvalidPassword() {
        setAdminUser()
        updateUserFieldJson('password', 'empty', '')
    }

    @Test
    void updateWithInvalidPasswordToken() {
        setAdminUser()
        updateUserFieldJson('passwordToken', 'long', String.randomString(User.PASSWORD_TOKEN_SIZE + 1))
    }

    @Test
    void updateWithInvalidCloneUserUid() {
        setAdminUser()
        updateUserFieldJson('cloneUserUid', 'format', 'NOT_A_UID')
        updateUserFieldJson('cloneUserUid', 'notFound', 'T7H2LBR4OYPU')
    }

    @Test
    void updateWithInvalidGroups() {
        setAdminUser()
        updateUserFieldJson('groups', 'notFound', 'DOES_NOT_EXIST')
    }

    /**
     * Submits a single User field value and tests the result. An error is expected.
     *
     * @param field that is being updated
     * @param code expected upon error
     * @param value to submit
     */
    def updateUserFieldJson(field, code, value) {
        updateInvalidFieldJson("/users/36C95YJJALQP", field, code, value, 3.6)
    }
}
