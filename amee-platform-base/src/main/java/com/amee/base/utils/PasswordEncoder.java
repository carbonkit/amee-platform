package com.amee.base.utils;

import org.mindrot.jbcrypt.BCrypt;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * A utility class to generate passwords.
 */
public class PasswordEncoder {

    public static void main(String[] args) throws Exception{

        System.out.println("Please type a plain-text password:");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input;
        while((input = reader.readLine()) != null && input.length() != 0) {
            System.out.println(BCrypt.hashpw(input.trim(), BCrypt.gensalt()));
        }
    }
}
