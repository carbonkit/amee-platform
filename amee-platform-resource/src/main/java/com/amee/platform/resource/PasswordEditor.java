package com.amee.platform.resource;

import org.mindrot.jbcrypt.BCrypt;

import java.beans.PropertyEditorSupport;

/**
 * Created by david on 26/01/2016.
 *
 * Custom editor to encode a submitted plaintext password.
 */
public class PasswordEditor extends PropertyEditorSupport {

    @Override
    public void setAsText(String text) {
        if (text.isEmpty()) {
            setValue("");
        } else {
            setValue(BCrypt.hashpw(text.trim(), BCrypt.gensalt()));
        }
    }
}
