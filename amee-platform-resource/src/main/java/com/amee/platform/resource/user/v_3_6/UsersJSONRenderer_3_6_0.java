package com.amee.platform.resource.user.v_3_6;

import com.amee.base.domain.Since;
import com.amee.base.resource.ResponseHelper;
import com.amee.platform.resource.user.UserResource;
import com.amee.platform.resource.user.UsersResource;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * Created by david on 15/02/2016.
 */
@Service
@Scope("prototype")
@Since("3.6.0")
public class UsersJSONRenderer_3_6_0 implements UsersResource.Renderer {

    private JSONObject rootObj;
    private JSONArray usersArr;

    @Override
    public void start() {
        rootObj = new JSONObject();
        usersArr = new JSONArray();
        ResponseHelper.put(rootObj, "users", usersArr);
    }

    @Override
    public void ok() {
        ResponseHelper.put(rootObj, "status", "OK");
    }

    @Override
    public void newUser(UserResource.Renderer renderer) {
        try {
            usersArr.put(((JSONObject) renderer.getObject()).getJSONObject("user"));
        } catch (JSONException e) {
            throw new RuntimeException("Caught JSONException: " + e.getMessage(), e);
        }
    }

    public void setTruncated(boolean truncated) {
        ResponseHelper.put(rootObj, "resultsTruncated", truncated);
    }

    public String getMediaType() {
        return "application/json";
    }

    public JSONObject getObject() {
        return rootObj;
    }
}
