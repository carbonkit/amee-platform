package com.amee.platform.resource.user.v_3_6;

import com.amee.base.domain.Since;
import com.amee.platform.resource.user.UserResource;
import com.amee.platform.resource.user.UsersResource;
import org.jdom2.Document;
import org.jdom2.Element;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * Created by david on 15/02/2016.
 */
@Service
@Scope("prototype")
@Since("3.6.0")
public class UsersDOMRenderer_3_6_0 implements UsersResource.Renderer {

    private Element rootElem;
    private Element usersElem;

    @Override
    public void start() {
        rootElem = new Element("Representation");
        usersElem = new Element("Users");
        rootElem.addContent(usersElem);
    }

    public void ok() {
        rootElem.addContent(new Element("Status").setText("OK"));
    }


    @Override
    public void newUser(UserResource.Renderer renderer) {
        usersElem.addContent(((Document) renderer.getObject()).getRootElement().getChild("User").detach());
    }

    public void setTruncated(boolean truncated) {
        usersElem.setAttribute("truncated", "" + truncated);
    }

    public String getMediaType() {
        return "application/xml";
    }

    public Document getObject() {
        return new Document(rootElem);
    }
}
