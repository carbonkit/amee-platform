package com.amee.platform.resource.user.v_3_6;

import com.amee.base.domain.Since;
import com.amee.base.validation.BaseValidator;
import com.amee.base.validation.ValidationSpecification;
import com.amee.domain.auth.Group;
import com.amee.domain.auth.User;
import com.amee.platform.resource.CSVListEditor;
import com.amee.platform.resource.PasswordEditor;
import com.amee.platform.resource.user.UserResource;
import com.amee.service.auth.AuthenticationService;
import com.amee.service.auth.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@Scope("prototype")
@Since("3.6.0")
public class UserValidator_3_6_0 extends BaseValidator implements UserResource.UserValidator {

    protected User user;
    protected Set<String> allowedFields = new HashSet<>();

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private GroupService groupService;

    public UserValidator_3_6_0() {
        super();
    }

    @Override
    public void initialise() {
        addEmail();
        addName();
        addUsername();
        addPassword();
        addPasswordToken();
        addGroups();
    }

    @Override
    public String getName() {
        return "user";
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.isAssignableFrom(clazz);
    }

    @Override
    public String[] getAllowedFields() {
        return allowedFields.toArray(new String[allowedFields.size()]);
    }

    @Override
    public User getObject() {
        return user;
    }

    @Override
    public void setObject(User user) {
        this.user = user;
    }

    protected void addEmail() {
        allowedFields.add("email");
        add(new ValidationSpecification()
                .setName("email")
                .setMaxSize(User.EMAIL_SIZE)
                .setAllowEmpty(false)
                .setCustomValidation(new ValidationSpecification.CustomValidation() {
                    @Override
                    public int validate(Object object, Object value, Errors errors) {

                        // Ensure User is unique on email.
                        User thisUser = (User) object;
                        if (thisUser != null) {
                            if (!authenticationService.isUserUniqueByEmail(thisUser)) {
                                errors.rejectValue("email", "duplicate");
                            }
                        }
                        return ValidationSpecification.CONTINUE;
                    }
                }));
    }

    protected void addName() {
        allowedFields.add("name");
        add(new ValidationSpecification()
                .setName("name")
                .setMaxSize(User.NAME_SIZE)
                .setAllowEmpty(false));
    }

    protected void addUsername() {
        allowedFields.add("username");
        add(new ValidationSpecification()
                .setName("username")
                .setMaxSize(User.NAME_SIZE)
                .setFormat("[A-Za-z][A-Za-z0-9_]+")
                .setAllowEmpty(false)
                .setCustomValidation(
                        new ValidationSpecification.CustomValidation() {
                            @Override
                            public int validate(Object object, Object value, Errors errors) {

                                // Ensure User is unique on username.
                                User thisUser = (User) object;
                                if (thisUser != null) {
                                    if (!authenticationService.isUserUniqueByUsername(thisUser)) {
                                        errors.rejectValue("username", "duplicate");
                                    }
                                }
                                return ValidationSpecification.CONTINUE;
                            }
                        }));
    }

    protected void addPassword() {
        allowedFields.add("password");
        addCustomEditor(String.class, "password", new PasswordEditor());
        add(new ValidationSpecification()
                .setName("password")
                .setAllowEmpty(false));
    }

    protected void addPasswordToken() {
        allowedFields.add("passwordToken");
        addCustomEditor(String.class, "passwordToken", new StringTrimmerEditor(true));
        add(new ValidationSpecification()
                .setName("passwordToken")
                .setMaxSize(User.PASSWORD_TOKEN_SIZE)
                .setAllowEmpty(true));
    }

    protected void addGroups() {
        allowedFields.add("cloneUserUid");
        add(new ValidationSpecification()
                .setName("cloneUserUid")
                .setUid(true)
                .setAllowEmpty(true)
                .setCustomValidation(new ValidationSpecification.CustomValidation() {
                    @Override
                    public int validate(Object object, Object value, Errors errors) {
                        if (value != null) {
                            User clone = authenticationService.getUserByIdentifier((String) value);
                            if (clone == null) {
                                errors.rejectValue("cloneUserUid", "notFound");
                            }
                        }
                        return ValidationSpecification.CONTINUE;
                    }
                }));

        allowedFields.add("groups");
        addCustomEditor(List.class, "groups", new CSVListEditor());
        add(new ValidationSpecification()
                .setName("groups")
                .setAllowEmpty(true)
                .setCustomValidation(new ValidationSpecification.CustomValidation() {
                    @Override
                    public int validate(Object object, Object value, Errors errors) {
                        if (value != null) {

                            @SuppressWarnings("unchecked")
                            List<String> groups = (List<String>) value;
                            for (String groupName : groups) {
                                groupName = groupName.trim();
                                Group group = groupService.getGroupByName(groupName);
                                if (group == null) {

                                    // This means it will fail at the fist unknown group name
                                    errors.rejectValue("groups", "notFound", new Object[]{groupName}, null);
                                }
                            }
                        }
                        return ValidationSpecification.CONTINUE;
                    }
                }));
    }

    // TODO: Add other fields (user type, locale?, time zone

    @Override
    protected void beforeBind(Map<String, String> values) {
        if (values.containsKey("email")) {
            values.put("email", values.get("email").toLowerCase());
        }
    }
}
