package com.amee.platform.resource.user;

import com.amee.base.validation.BaseValidator;
import com.amee.base.validation.ValidationSpecification;
import com.amee.domain.UsersFilter;
import com.amee.domain.auth.User;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("prototype")
public class UsersFilterValidator extends BaseValidator {

    public UsersFilterValidator() {
        super();
        addEmail();
        addPasswordToken();
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return UsersFilter.class.isAssignableFrom(clazz);
    }

    private void addEmail() {
        add(new ValidationSpecification()
                .setName("email")
                .setMaxSize(User.EMAIL_SIZE)
                .setAllowEmpty(true));
    }

    private void addPasswordToken() {
        add(new ValidationSpecification()
                .setName("passwordToken")
                .setAllowEmpty(true));
    }
}
