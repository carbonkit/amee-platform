package com.amee.platform.resource.user.v_3_6;

import com.amee.base.domain.Since;
import com.amee.base.resource.RequestWrapper;
import com.amee.base.resource.ResourceBeanFinder;
import com.amee.base.resource.ResponseHelper;
import com.amee.base.transaction.AMEETransaction;
import com.amee.base.validation.ValidationException;
import com.amee.domain.auth.User;
import com.amee.platform.resource.ResourceService;
import com.amee.platform.resource.user.UserResource;
import com.amee.service.auth.ResourceAuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by david on 11/02/2016.
 */
@Service
@Scope("prototype")
@Since("3.6.0")
public class UserFormAcceptor_3_6_0 implements UserResource.FormAcceptor {

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private ResourceAuthorizationService resourceAuthorizationService;

    @Autowired
    private ResourceBeanFinder resourceBeanFinder;

    @Override
    @AMEETransaction
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Object handle(RequestWrapper requestWrapper) {
        User user = resourceService.getUser(requestWrapper);

        resourceAuthorizationService.ensureAuthorizedForAccept(
                requestWrapper.getAttributes().get("activeUserUid"), user);

        UserResource.UserValidator validator = getValidator(requestWrapper);
        validator.setObject(user);
        validator.initialise();

        if (validator.isValid(requestWrapper.getFormParameters())) {
            return ResponseHelper.getOK(requestWrapper, null, user.getUid());
        } else {
            throw new ValidationException(validator.getValidationResult());
        }
    }

    @Override
    public UserResource.UserValidator getValidator(RequestWrapper requestWrapper) {
        return (UserResource.UserValidator) resourceBeanFinder.getValidator(UserResource.UserValidator.class, requestWrapper);
    }

}
