package com.amee.platform.resource.user.v_3_6;

import com.amee.base.domain.Since;
import com.amee.base.resource.RequestWrapper;
import com.amee.base.resource.ResponseHelper;
import com.amee.base.transaction.AMEETransaction;
import com.amee.domain.auth.User;
import com.amee.platform.resource.ResourceService;
import com.amee.platform.resource.user.UserResource;
import com.amee.service.auth.AuthenticationService;
import com.amee.service.auth.ResourceAuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by david on 27/01/2016.
 */
@Service
@Scope("prototype")
@Since("3.6.0")
public class UserRemover_3_6_0 implements UserResource.Remover {

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private ResourceAuthorizationService resourceAuthorizationService;

    @Override
    @AMEETransaction
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Object handle(RequestWrapper requestWrapper) {

        // Get entities
        User user = resourceService.getUser(requestWrapper);

        // Authorised?
        resourceAuthorizationService.ensureAuthorizedForRemove(requestWrapper.getAttributes().get("activeUserUid"), user);

        // Remove the user
        authenticationService.remove(user);
        return ResponseHelper.getOK(requestWrapper, null, user.getUid());
    }
}
