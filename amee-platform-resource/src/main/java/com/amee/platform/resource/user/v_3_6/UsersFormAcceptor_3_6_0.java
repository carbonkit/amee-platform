package com.amee.platform.resource.user.v_3_6;

import com.amee.base.domain.Since;
import com.amee.base.resource.RequestWrapper;
import com.amee.base.resource.ResourceBeanFinder;
import com.amee.base.resource.ResponseHelper;
import com.amee.base.transaction.AMEETransaction;
import com.amee.base.validation.ValidationException;
import com.amee.domain.auth.Group;
import com.amee.domain.auth.GroupPrincipal;
import com.amee.domain.auth.User;
import com.amee.platform.resource.ResourceService;
import com.amee.platform.resource.user.UserResource;
import com.amee.platform.resource.user.UsersResource;
import com.amee.service.auth.AuthenticationService;
import com.amee.service.auth.GroupService;
import com.amee.service.auth.ResourceAuthorizationService;
import com.amee.service.data.DataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by david on 19/01/2016.
 */
@Service
@Scope("prototype")
@Since("3.6.0")
public class UsersFormAcceptor_3_6_0 implements UsersResource.FormAcceptor {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private ResourceAuthorizationService resourceAuthorizationService;

    @Autowired
    private ResourceBeanFinder resourceBeanFinder;

    @Autowired
    ResourceService resourceService;

    @Autowired
    DataService dataService;

    @Override
    @AMEETransaction
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Object handle(RequestWrapper requestWrapper) {

        // Authorised?
        resourceAuthorizationService.ensureAuthorizedForAccept(requestWrapper.getAttributes().get("activeUserUid"));

        // Create the new user
        User user = new User();

        // TODO: Get rid of this
        user.setAPIVersion(dataService.getAPIVersion("2.0"));
        authenticationService.persist(user);
        return handle(requestWrapper, user);
    }

    @Override
    public Object handle(RequestWrapper requestWrapper, User user) {

        // Create validator
        UserResource.UserValidator validator = getValidator(requestWrapper);
        validator.setObject(user);
        validator.initialise();

        // Is the user valid?
        if (validator.isValid(requestWrapper.getFormParameters())) {

            // We can either 'clone' Group membership from an existing User *OR* join specified Groups.
            // Was a clone User supplied?
            User cloneUser = authenticationService.getUserByUid(user.getCloneUserUid());
            if (cloneUser != null) {
                // Clone User was supplied.
                // Clone Group memberships.
                log.info("Cloning groups from user {}", cloneUser.getUid());
                for (GroupPrincipal groupPrincipal : groupService.getGroupPrincipalsForPrincipal(cloneUser)) {
                    GroupPrincipal newGroupPrincipal = new GroupPrincipal(groupPrincipal.getGroup(), user);
                    groupService.save(newGroupPrincipal);
                }
            } else {
                // Clone User was NOT supplied.
                // Look for requested Groups to join.
                if (user.getGroups() != null) {
                    for (String groupName : user.getGroups()) {
                        groupName = groupName.trim();
                        Group group = groupService.getGroupByName(groupName);
                        if (group != null) {
                            log.info("Adding user to group '{}'", group.getName());
                            GroupPrincipal newGroupPrincipal = new GroupPrincipal(group, user);
                            groupService.save(newGroupPrincipal);
                        } else {
                            // This shouldn't happen if we check the groups in the validator.
                            log.warn("Unable to find requested group: '" + groupName + "'");
                        }
                    }
                }
            }

            String location = "/" + requestWrapper.getVersion() + "/users/" + user.getFullPath();
            return ResponseHelper.getOK(requestWrapper, location, user.getUid());
        } else {
            throw new ValidationException(validator.getValidationResult());
        }
    }

    @Override
    public UserResource.UserValidator getValidator(RequestWrapper requestWrapper) {
        return (UserResource.UserValidator)
                resourceBeanFinder.getValidator(
                        UserResource.UserValidator.class, requestWrapper);
    }
}
