package com.amee.platform.resource.user;

import com.amee.base.validation.BaseValidator;
import com.amee.domain.UsersFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.validation.Validator;

import java.util.HashSet;
import java.util.Set;

@Service
@Scope("prototype")
public class UsersFilterValidationHelper extends BaseValidator {

    @Autowired
    private UsersFilterValidator validator;

    private UsersFilter usersFilter;
    private Set<String> allowedFields;

    @Override
    public Object getObject() {
        return usersFilter;
    }

    @Override
    public Validator getValidator() {
        return validator;
    }

    @Override
    public String getName() {
        return "usersFilter";
    }

    @Override
    public String[] getAllowedFields() {
        if (allowedFields == null) {
            allowedFields = new HashSet<>();
            allowedFields.add("email");
            allowedFields.add("passwordToken");
        }
        return allowedFields.toArray(new String[allowedFields.size()]);
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return false;
    }

    public UsersFilter getUsersFilter() {
        return usersFilter;
    }

    public void setUsersFilter(UsersFilter usersFilter) {
        this.usersFilter = usersFilter;
    }
}
