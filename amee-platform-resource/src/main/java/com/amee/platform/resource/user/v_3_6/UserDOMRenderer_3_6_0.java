package com.amee.platform.resource.user.v_3_6;

import com.amee.base.domain.Since;
import com.amee.domain.auth.User;
import com.amee.platform.resource.user.UserResource;
import org.jdom2.Document;
import org.jdom2.Element;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * Created by david on 27/01/2016.
 */
@Service
@Scope("prototype")
@Since("3.6.0")
public class UserDOMRenderer_3_6_0 implements UserResource.Renderer {

    protected User user;
    protected Element rootElem;
    protected Element userElem;

    @Override
    public void start() {
        rootElem = new Element("Representation");
    }

    @Override
    public void ok() {
        rootElem.addContent(new Element("Status").setText("OK"));
    }

    @Override
    public void newUser(User user) {
        this.user = user;
        userElem = new Element("User");
        if (rootElem != null) {
            rootElem.addContent(userElem);
        }
    }

    @Override
    public void addBasic() {
        userElem.setAttribute("uid", user.getUid());
        userElem.addContent(new Element("Username").setText(user.getUsername()));
    }

    @Override
    public void addName() {
        userElem.addContent(new Element("Name").setText(user.getName()));
    }

    @Override
    public void addEmail() {
        userElem.addContent(new Element("Email").setText(user.getEmail()));
    }

    public void addAudit() {
        userElem.setAttribute("status", user.getStatus().getName());
        userElem.setAttribute("created", DATE_FORMAT.print(user.getCreated().getTime()));
        userElem.setAttribute("modified", DATE_FORMAT.print(user.getModified().getTime()));
    }

    @Override
    public String getMediaType() {
        return "application/xml";
    }

    @Override
    public Object getObject() {
        return new Document(rootElem);
    }

}
