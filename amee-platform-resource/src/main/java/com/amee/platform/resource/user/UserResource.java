package com.amee.platform.resource.user;

import com.amee.base.resource.*;
import com.amee.domain.auth.User;

import java.util.Map;

/**
 * Created by david on 17/01/2016.
 */
public interface UserResource {

    interface Builder extends ResourceBuilder {

        void handle(RequestWrapper requestWrapper, User user);

        UserResource.Renderer getRenderer(RequestWrapper requestWrapper);
    }

    interface Renderer extends ResourceRenderer {

        void newUser(User user);

        void addBasic();

        void addAudit();

        void addName();

        void addEmail();
    }

    interface FormAcceptor extends ResourceAcceptor {

        UserResource.UserValidator getValidator(RequestWrapper requestWrapper);
    }

    interface UserValidator {

        void initialise();

        boolean isValid(Map<String, String> queryParameters);

        User getObject();

        void setObject(User object);

        ValidationResult getValidationResult();
    }

    interface Remover extends ResourceRemover {

    }
}
