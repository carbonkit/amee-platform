package com.amee.platform.resource.user;

import com.amee.base.resource.RequestWrapper;
import com.amee.base.resource.ResourceAcceptor;
import com.amee.base.resource.ResourceBuilder;
import com.amee.base.resource.ResourceRenderer;
import com.amee.domain.auth.User;

public interface UsersResource {

    interface Builder extends ResourceBuilder {
        UsersResource.Renderer getRenderer(RequestWrapper requestWrapper);
    }

    interface Renderer extends ResourceRenderer {

        void newUser(UserResource.Renderer renderer);

        void setTruncated(boolean truncated);
    }

    interface FormAcceptor extends ResourceAcceptor {

        Object handle(RequestWrapper requestWrapper, User user);

        UserResource.UserValidator getValidator(RequestWrapper requestWrapper);
    }
}
