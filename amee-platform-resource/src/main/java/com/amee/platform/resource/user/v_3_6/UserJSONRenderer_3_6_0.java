package com.amee.platform.resource.user.v_3_6;

import com.amee.base.domain.Since;
import com.amee.base.resource.ResponseHelper;
import com.amee.domain.auth.User;
import com.amee.platform.resource.user.UserResource;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

/**
 * Created by david on 18/01/2016.
 */
@Service
@Scope("prototype")
@Since("3.6.0")
public class UserJSONRenderer_3_6_0 implements UserResource.Renderer {

    protected User user;
    protected JSONObject rootObj;
    protected JSONObject userObj;

    @Override
    public void start() {
        rootObj = new JSONObject();
    }

    @Override
    public void ok() {
        ResponseHelper.put(rootObj, "status", "OK");
    }

    @Override
    public void newUser(User user) {
        this.user = user;
        userObj = new JSONObject();
        if (rootObj != null) {
            ResponseHelper.put(rootObj, "user", userObj);
        }
    }

    @Override
    public void addBasic() {
        ResponseHelper.put(userObj, "uid", user.getUid());
        ResponseHelper.put(userObj, "username", user.getUsername());
    }

    @Override
    public void addAudit() {
        ResponseHelper.put(userObj, "status", user.getStatus().getName());
        ResponseHelper.put(userObj, "created", DATE_FORMAT.print(user.getCreated().getTime()));
        ResponseHelper.put(userObj, "modified", DATE_FORMAT.print(user.getModified().getTime()));
    }

    @Override
    public void addName() {
        ResponseHelper.put(userObj, "name", user.getName());
    }

    @Override
    public void addEmail() {
        ResponseHelper.put(userObj, "email", user.getEmail());
    }

    @Override
    public String getMediaType() {
        return "application/json";
    }

    @Override
    public Object getObject() {
        return rootObj;
    }
}
