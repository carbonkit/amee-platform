package com.amee.platform.resource.user.v_3_6;

import com.amee.base.domain.ResultsWrapper;
import com.amee.base.domain.Since;
import com.amee.base.resource.RequestWrapper;
import com.amee.base.resource.ResourceBeanFinder;
import com.amee.base.transaction.AMEETransaction;
import com.amee.base.validation.ValidationException;
import com.amee.domain.UsersFilter;
import com.amee.domain.auth.User;
import com.amee.platform.resource.user.UserResource;
import com.amee.platform.resource.user.UsersFilterValidationHelper;
import com.amee.platform.resource.user.UsersResource;
import com.amee.service.auth.AuthenticationService;
import com.amee.service.auth.ResourceAuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Scope("prototype")
@Since("3.6.0")
public class UsersBuilder_3_6_0 implements UsersResource.Builder {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private ResourceBeanFinder resourceBeanFinder;

    @Autowired
    private ResourceAuthorizationService resourceAuthorizationService;

    @Autowired
    private UsersFilterValidationHelper validationHelper;

    private UsersResource.Renderer usersRenderer;

    @Override
    @AMEETransaction
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Object handle(RequestWrapper requestWrapper) {

        // If you can create users then you can list them
        resourceAuthorizationService.ensureAuthorizedForAccept(requestWrapper.getAttributes().get("activeUserUid"));

        UsersFilter filter = new UsersFilter();
        validationHelper.setUsersFilter(filter);
        if (validationHelper.isValid(requestWrapper.getQueryParameters())) {
            UsersResource.Renderer renderer = getRenderer(requestWrapper);
            renderer.start();

            ResultsWrapper<User> resultsWrapper = authenticationService.getUsers(filter);
            renderer.setTruncated(resultsWrapper.isTruncated());

            UserResource.Builder userBuilder = getUserBuilder(requestWrapper);
            for (User user : resultsWrapper.getResults()) {
                userBuilder.handle(requestWrapper, user);
                renderer.newUser(userBuilder.getRenderer(requestWrapper));
            }

            renderer.ok();
            return renderer.getObject();
        } else {
            throw new ValidationException(validationHelper.getValidationResult());
        }
    }

    @Override
    public UsersResource.Renderer getRenderer(RequestWrapper requestWrapper) {
        if (usersRenderer == null) {
            usersRenderer = (UsersResource.Renderer) resourceBeanFinder.getRenderer(UsersResource.Renderer.class, requestWrapper);
        }
        return usersRenderer;
    }

    private UserResource.Builder getUserBuilder(RequestWrapper requestWrapper) {
        return (UserResource.Builder) resourceBeanFinder.getBuilder(UserResource.Builder.class, requestWrapper);
    }
}
