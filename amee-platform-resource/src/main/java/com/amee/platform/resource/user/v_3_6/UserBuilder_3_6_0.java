package com.amee.platform.resource.user.v_3_6;

import com.amee.base.domain.Since;
import com.amee.base.resource.RequestWrapper;
import com.amee.base.resource.ResourceBeanFinder;
import com.amee.base.transaction.AMEETransaction;
import com.amee.domain.auth.User;
import com.amee.platform.resource.ResourceService;
import com.amee.platform.resource.user.UserResource;
import com.amee.service.auth.AuthenticationService;
import com.amee.service.auth.ResourceAuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by david on 17/01/2016.
 */
@Service
@Scope("prototype")
@Since("3.6.0")
public class UserBuilder_3_6_0 implements UserResource.Builder {

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private ResourceBeanFinder resourceBeanFinder;

    @Autowired
    private ResourceAuthorizationService resourceAuthorizationService;

    private UserResource.Renderer renderer;

    @Override
    @AMEETransaction
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Object handle(RequestWrapper requestWrapper) {

        // Get resource entities for this request.
        User user = resourceService.getUser(requestWrapper);

        // Authorised for this user?
        resourceAuthorizationService.ensureAuthorizedForBuild(
                requestWrapper.getAttributes().get("activeUserUid"), user);

        // Handle the user
        handle(requestWrapper, user);
        UserResource.Renderer renderer = getRenderer(requestWrapper);
        renderer.ok();
        return renderer.getObject();
    }

    @Override
    public void handle(RequestWrapper requestWrapper, User user) {

        // Get the renderer
        UserResource.Renderer renderer = getRenderer(requestWrapper);
        renderer.start();

        // Collect rendering options from matrix paramters
        boolean full = requestWrapper.getMatrixParameters().containsKey("full");
        boolean audit = requestWrapper.getMatrixParameters().containsKey("audit");
        boolean name = requestWrapper.getMatrixParameters().containsKey("name");
        boolean email = requestWrapper.getMatrixParameters().containsKey("email");

        // New user + basic
        renderer.newUser(user);
        renderer.addBasic();

        // Optionals
        if (audit || full) {
            renderer.addAudit();
        }

        if (name || full) {
            renderer.addName();
        }

        if (email || full) {
            renderer.addEmail();
        }
    }

    @Override
    public UserResource.Renderer getRenderer(RequestWrapper requestWrapper) {
        if (renderer == null) {
            renderer = (UserResource.Renderer) resourceBeanFinder.getRenderer(UserResource.Renderer.class, requestWrapper);
        }
        return renderer;
    }
}
